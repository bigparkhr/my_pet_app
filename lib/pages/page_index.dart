import 'package:flutter/material.dart';


class PageIndex extends StatelessWidget {
  const PageIndex({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          '마이 럽 김몽몽',
          style: TextStyle(fontSize: 30, color: Colors.white),
        ),
        centerTitle: true,
        backgroundColor: Colors.greenAccent,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Image.asset('assets/kim1.jpeg',width: 550, height: 300, fit: BoxFit.fill),
            Container(
              child: Column(
                children: [
                  Text('김몽몽이를 소개합니다', style: TextStyle(fontWeight: FontWeight.bold, letterSpacing: 2.5, height: 2.5),),
                  Text('김몽몽이는 아주 이케멘이에요', style: TextStyle(letterSpacing: 2.5, height: 2.5),),
                  Text('매우 카와이', style: TextStyle(letterSpacing: 2.5, height: 2.5),),
                ],
              ),
            ),
            Container(
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset('assets/kim2.jpeg',width: 300, height: 250, fit: BoxFit.cover, alignment: Alignment.center),
                      Image.asset('assets/kim3.jpeg',width: 300, height: 250, fit: BoxFit.cover, alignment: Alignment.center),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset('assets/kim4.jpeg',width: 300, height: 250, fit: BoxFit.cover, alignment: Alignment.center),
                      Image.asset('assets/kim5.jpeg',width: 300, height: 250, fit: BoxFit.cover, alignment: Alignment.center),
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
